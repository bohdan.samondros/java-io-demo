package com.mycompany.app;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class ScanDemo {
    public static void main(String[] args) throws IOException {

//        demoWithFile();
        demoWithSystemIn();
//        demoWithLocale();
    }

    private static void demoWithFile() throws FileNotFoundException {
        Scanner s = null;

        try {
            s = new Scanner(new BufferedReader(new FileReader("multiline.txt")));

            while (s.hasNext()) {
                System.out.println(s.next());
            }

/*            while (s.hasNextLine()) {
                System.out.println(s.nextLine());
            }*/
        } finally {
            if (s != null) {
                s.close();
            }
        }
    }

    public static void demoWithSystemIn() {

        Scanner s = null;

        try {
            s = new Scanner(System.in);

            System.out.println(s.nextLine());
        } finally {
            if (s != null) {
                s.close();
            }
        }
    }

    public static void demoWithLocale() {

        Scanner s = null;

        try {
            s = new Scanner(System.in);
            s.useLocale(Locale.FRANCE);
//            s.useLocale(Locale.US);

//            System.out.println(s.nextInt());
            System.out.println(s.nextDouble());
        } finally {
            if (s != null) {
                s.close();
            }
        }
    }
}