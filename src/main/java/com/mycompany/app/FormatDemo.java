package com.mycompany.app;

import java.util.Calendar;
import java.util.Locale;

public class FormatDemo {
/*        public static void main(String[] args) {
            int i = 2;
            double r = Math.sqrt(i);

            System.out.format("The square root of %d is %f.%n", i, r);
        }*/

    public static void main(String[] args) {
        long n = 461012L;
        System.out.println(Locale.getDefault());
        System.out.format("%d%n", n);      //  -->  "461012"
        System.out.format("%08d%n", n);    //  -->  "00461012"
        System.out.format("%+8d%n", n);    //  -->  " +461012"
        System.out.format("%,8d%n", n);    // -->  " 461,012"
        System.out.format("%+,8d%n%n", n); //  -->  "+461,012"

        double pi = Math.PI;

        System.out.format("%f, %1$+020.10f %n", Math.PI);
        System.out.format("%f%n", pi);       // -->  "3.141593"
        System.out.format("%.3f%n", pi);     // -->  "3.142"
        System.out.format("%10.3f%n", pi);   // -->  "     3.142"
        System.out.format(Locale.ENGLISH,"%-10.3f%n", pi);  // -->  "3.142"
        System.out.format(Locale.FRANCE,
                "%-10.4f%n%n", pi); // -->  "3,1416"

        Calendar c = Calendar.getInstance();
        System.out.format("%tB %<te, %<tY%n", c); // -->  "May 29, 2006"

        System.out.format("%tl:%<tM %<tp%n", c);  // -->  "2:34 am"

        System.out.format("%tD%n", c);    // -->  "05/29/06"
    }
}
