package com.mycompany.app;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class SeekableByteChannelDemo {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("F:\\Work\\my-app","multiline.txt");
        SeekableByteChannel sbc = Files.newByteChannel(path, StandardOpenOption.READ);
        ByteBuffer bf = ByteBuffer.allocate(1024);
        int i = 0;
        while((i=sbc.read(bf))>0){
            bf.flip();
            System.out.println(i);
            bf.clear();

        }
    }
}
