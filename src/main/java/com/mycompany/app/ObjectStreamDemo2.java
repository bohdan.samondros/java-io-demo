package com.mycompany.app;

import java.io.*;

public class ObjectStreamDemo2 {
    static final String dataFile = "invoicedata";

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        A a = new A();
        B b = new B();
        a.b = b;
        b.a = a;

        System.out.println(a);
        System.out.println(b);
        System.out.println(a.b);
        System.out.println(b.a);
        try (ObjectOutputStream out = new ObjectOutputStream(new
                BufferedOutputStream(new FileOutputStream(dataFile)))) {
            out.writeObject(a);
            out.writeObject(b);
        }

        try (ObjectInputStream in = new ObjectInputStream(new
                BufferedInputStream(new FileInputStream(dataFile)))) {

            A a2 = (A) in.readObject();
            B b2 = (B) in.readObject();
            System.out.println(a2);
            System.out.println(b2);
            System.out.println(a2.b);
            System.out.println(b2.a);
        }


    }


    public static class A implements Serializable {
        B b;
    }

    public static class B implements Serializable {
        A a;
    }
}
