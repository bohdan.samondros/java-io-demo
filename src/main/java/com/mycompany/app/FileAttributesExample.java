package com.mycompany.app;

import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.DosFileAttributes;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermissions;

public class FileAttributesExample {
    public static void main(String[] args) throws IOException {
        System.out.println("BASIC ATTRIBUTES");
        Path file = Paths.get("C:/Users/Bohdan/.bash_history");
        BasicFileAttributes basicAttrs = Files.readAttributes(file, BasicFileAttributes.class);

        System.out.println("creationTime: " + basicAttrs.creationTime());
        System.out.println("lastAccessTime: " + basicAttrs.lastAccessTime());
        System.out.println("lastModifiedTime: " + basicAttrs.lastModifiedTime());

        System.out.println("isDirectory: " + basicAttrs.isDirectory());
        System.out.println("isOther: " + basicAttrs.isOther());
        System.out.println("isRegularFile: " + basicAttrs.isRegularFile());
        System.out.println("isSymbolicLink: " + basicAttrs.isSymbolicLink());
        System.out.println("size: " + basicAttrs.size());

        System.out.println("DOS ATTRIBUTES");

        try {
            DosFileAttributes dosAttrs =
                    Files.readAttributes(file, DosFileAttributes.class);
            System.out.println("isReadOnly is " + dosAttrs.isReadOnly());
            System.out.println("isHidden is " + dosAttrs.isHidden());
            System.out.println("isArchive is " + dosAttrs.isArchive());
            System.out.println("isSystem is " + dosAttrs.isSystem());
        } catch (UnsupportedOperationException x) {
            System.err.println("DOS file" +
                    " attributes not supported:" + x);
        }

/*        System.out.println("POSIX ATTRIBUTES");

        PosixFileAttributes attr =
                Files.readAttributes(file, PosixFileAttributes.class);
        System.out.format("%s %s %s%n",
                attr.owner().getName(),
                attr.group().getName(),
                PosixFilePermissions.toString(attr.permissions()));*/

        System.out.println("FILE STORE ATTRIBUTES");
        FileStore store = Files.getFileStore(file);

        long total = store.getTotalSpace() / 1024;
        long used = (store.getTotalSpace() -
                store.getUnallocatedSpace()) / 1024;
        long avail = store.getUsableSpace() / 1024;
        System.out.format("Total: %,d Kb %n", total);
        System.out.format("Used: %,d Kb %n", used);
        System.out.format("Avail: %,d Kb %n", avail);
    }
}
