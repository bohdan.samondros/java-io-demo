package com.mycompany.app;

import java.io.File;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        FileReader fileReader = new FileReader("file.txt");
        FileReader fileReader1 = new FileReader(new File("file.txt"));

        // since Java 11
        FileReader fileReader2 = new FileReader(new File("file.txt"), StandardCharsets.UTF_8);
    }
}
